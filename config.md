# Table of Contents

1.  [User Setup](#org9251b65)
2.  [Emacs Server](#orgc29cc83)
3.  [Theme](#org2c39afc)
    1.  [Fonts](#org671f8c2)
        1.  [Font Pretty Code Mode](#orgb9eb8d6)
        2.  [Font Ligatures](#org29f2cc9)
        3.  [Font Ligatures for Org mode](#org5af31ca)
    2.  [Line Numbers](#org74ab7ee)
4.  [Org Mode](#orgde84b48)
    1.  [Setting Org Directory](#orgf041bb4)
    2.  [Org Roam](#org3b050a3)
        1.  [Setting Org Roam Directory](#org247331a)
        2.  [Keybindings for Org Roam](#org8e09e5c)
        3.  [Company Org Roam Setup](#orgbfefe26)
        4.  [Org Journal Setup](#orgde4e16e)
        5.  [Deft Setup - For searching roam notes](#orga25edf8)
        6.  [Org Roam Protocol Setup](#org91cec3b)
        7.  [Org Roam Graph](#org2a1e26a)
    3.  [Org Babel](#orge221a30)
        1.  [Setting up languages](#org9523c9f)
5.  [Javascript / Typescript](#orgb81b47f)
    1.  [Prettier Setup](#orgc9548b2)
    2.  [TIDE Setup](#org65bc316)
        1.  [Company Tooltip](#org55be22a)
        2.  [Tide Formatting](#org8446710)
        3.  [TSX Setup](#org0ef2667)
        4.  [Typescript TS-Lint](#org74fa200)
        5.  [JS TIDE Setup](#org45217c7)
        6.  [JSX TIDE Setup](#org6b97cf9)
        7.  [Flycheck](#orgca829df)
        8.  [TIDE USE](#org16304e2)
6.  [Custom Set Variables](#orge6beef5)
7.  [Common Lisp Setup](#org385d4e6)



<a id="org9251b65"></a>

# User Setup

    (setq user-full-name "John Doe"
          user-mail-address "some@some.com")


<a id="orgc29cc83"></a>

# Emacs Server

    (server-start)


<a id="org2c39afc"></a>

# Theme

    (setq
     doom-theme 'zaiste
     )


<a id="org671f8c2"></a>

## Fonts

    (setq doom-font (font-spec :family "Iosevka" :size 15 :weight 'semi-light)
           doom-variable-pitch-font (font-spec :family "Iosevka" :size 15))


<a id="orgb9eb8d6"></a>

### Font Pretty Code Mode

    (setq +pretty-code-enabled-modes '(emacs-lisp-mode org-mode clojure-mode
                                       latex-mode scheme-mode racket-mode ess-r-mode))
    (setq highlight-indent-guides-responsive 'top
          highlight-indent-guides-delay 0)


<a id="org29f2cc9"></a>

### Font Ligatures

    (setq +pretty-code-iosevka-font-ligatures
          (append +pretty-code-iosevka-font-ligatures
                  '(("[ ]" .  "☐")
                    ("[X]" . "☑" )
                    ("[-]" . "❍" )
                    ("%>%" . ?▷)
                    ("%$%" . ?◇)
                    ("%T>%" . ?▶)
                    ("function" . ?ƒ))))


<a id="org5af31ca"></a>

### Font Ligatures for Org mode

    (defface org-checkbox-done-text
      '((t (:foreground "#5a637b")))
      "Face for the text part of a checked org-mode checkbox.")
    
    (font-lock-add-keywords 'org-mode
                            '(("^[ \t]*\\(?:[-+*]\\|[0-9]+[).]\\)[ \t]+\\(\\(?:\\[@\\(?:start:\\)?[0-9]+\\][ \t]*\\)?\\[\\(?:X\\|\\([0-9]+\\)/\\2\\)\\][^\n]*\n\\)"
                               1 'org-checkbox-done-text prepend))
                            'append)
    ;; (custom-set-faces '(org-checkbox ((t (:foreground nil :inherit org-todo)))))


<a id="org74ab7ee"></a>

## Line Numbers

    (setq display-line-numbers 'relative)
    (setq display-line-numbers-type 'relative)


<a id="orgde84b48"></a>

# Org Mode


<a id="orgf041bb4"></a>

## Setting Org Directory

    (setq org-directory "~/org/")


<a id="org3b050a3"></a>

## Org Roam


<a id="org247331a"></a>

### Setting Org Roam Directory

    (setq org-roam-directory "~/Downloads/Desktop/Notes/")


<a id="org8e09e5c"></a>

### Keybindings for Org Roam

    (after! org-roam
            (map! :leader
                :prefix "n"
                :desc "org-roam" "l" #'org-roam
                :desc "org-roam-insert" "i" #'org-roam-insert
                :desc "org-roam-switch-to-buffer" "b" #'org-roam-switch-to-buffer
                :desc "org-roam-find-file" "f" #'org-roam-find-file
                :desc "org-roam-show-graph" "g" #'org-roam-show-graph
                :desc "org-roam-insert" "i" #'org-roam-insert
                :desc "org-roam-capture" "c" #'org-roam-capture))


<a id="orgbfefe26"></a>

### Company Org Roam Setup

    (require 'company-org-roam)
        (use-package company-org-roam
          :when (featurep! :completion company)
          :after org-roam
          :config
          (set-company-backend! 'org-mode '(company-org-roam company-yasnippet company-dabbrev)))


<a id="orgde4e16e"></a>

### Org Journal Setup

    (use-package org-journal
          :bind
          ("C-c n j" . org-journal-new-entry)
          :custom
          (org-journal-dir "~/Downloads/Desktop/Notes/")
          (org-journal-date-prefix "#+TITLE: ")
          (org-journal-file-format "%Y-%m-%d.org")
          (org-journal-date-format "%A, %d %B %Y"))
    (setq org-journal-enable-agenda-integration t)


<a id="orga25edf8"></a>

### Deft Setup - For searching roam notes

    (use-package deft
          :after org
          :bind
          ("C-c n d" . deft)
          :custom
          (deft-recursive t)
          (deft-use-filter-string-for-filename t)
          (deft-default-extension "org")
          (deft-directory "~/Downloads/Desktop/Notes/"))


<a id="org91cec3b"></a>

### Org Roam Protocol Setup

    (require 'org-roam-protocol)
    (after! org-roam
          (setq org-roam-capture-ref-templates
                '(("r" "ref" plain (function org-roam-capture--get-point)
                   "%?"
                   :file-name "websites/${slug}"
                   :head "#+TITLE: ${title}
        #+ROAM_KEY: ${ref}
        - source :: ${ref}"
                   :unnarrowed t))))


<a id="org2a1e26a"></a>

### Org Roam Graph

    (setq org-roam-graph-viewer "/Applications/Firefox.app/Contents/MacOS/firefox")


<a id="orge221a30"></a>

## Org Babel


<a id="org9523c9f"></a>

### Setting up languages

    (org-babel-do-load-languages
     'org-babel-load-languages
     '(
       (typescript . t)
       ))


<a id="orgb81b47f"></a>

# Javascript / Typescript


<a id="orgc9548b2"></a>

## Prettier Setup

    ;; ;;Setting up prettier
    ;; (use-package prettier-js
    ;;   :after js2-mode
    ;;   :init
    ;;   (add-hook 'js2-jsx-mode-hook 'prettier-js-mode)
    ;;   :config
    ;;   (setq prettier-js-args '("--trailing-comma" "all"
    ;;                            "--bracket-spacing" "false"
    ;;                            "--single-quote" "true"
    ;;                            "--no-semi" "true"
    ;;                            "--jsx-single-quote" "true"
    ;;                            "--jsx-bracket-same-line" "true"
    ;;                            "--print-width" "100")))


<a id="org65bc316"></a>

## TIDE Setup

    (defun setup-tide-mode ()
      (interactive)
      (tide-setup)
      (flycheck-mode +1)
      (setq flycheck-check-syntax-automatically '(save mode-enabled))
      (eldoc-mode +1)
      (tide-hl-identifier-mode +1)
      (company-mode +1))


<a id="org55be22a"></a>

### Company Tooltip

    (setq company-tooltip-align-annotations t)


<a id="org8446710"></a>

### Tide Formatting

    (add-hook 'before-save-hook 'tide-format-before-save)
    (add-hook 'typescript-mode-hook #'setup-tide-mode)


<a id="org0ef2667"></a>

### TSX Setup

    (require 'web-mode)
    (add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
    (add-hook 'web-mode-hook
              (lambda ()
                (when (string-equal "tsx" (file-name-extension buffer-file-name))
                  (setup-tide-mode))))


<a id="org74fa200"></a>

### Typescript TS-Lint

    ;; (flycheck-add-mode 'typescript-tslint 'web-mode)


<a id="org45217c7"></a>

### JS TIDE Setup

    (add-hook 'js2-mode-hook #'setup-tide-mode)
    ;; (flycheck-add-next-checker 'javascript-eslint 'javascript-tide 'append)


<a id="org6b97cf9"></a>

### JSX TIDE Setup

    (require 'web-mode)
    (add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
    (add-hook 'web-mode-hook
              (lambda ()
                (when (string-equal "jsx" (file-name-extension buffer-file-name))
                  (setup-tide-mode))))


<a id="orgca829df"></a>

### Flycheck

    (require 'web-mode)
    (add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
    (add-hook 'web-mode-hook
              (lambda ()
                (when (string-equal "jsx" (file-name-extension buffer-file-name))
                  (setup-tide-mode))))
    ;; configure jsx-tide checker to run after your default jsx checker
    ;; (flycheck-add-mode 'javascript-eslint 'web-mode)
    ;; (flycheck-add-next-checker 'javascript-eslint 'jsx-tide 'append)


<a id="org16304e2"></a>

### TIDE USE

    (use-package tide
      :ensure t
      :after (typescript-mode company flycheck)
      :hook ((typescript-mode . tide-setup)
             (typescript-mode . tide-hl-identifier-mode)
             (before-save . tide-format-before-save)))


<a id="orge6beef5"></a>

# Custom Set Variables

    (custom-set-variables
     '(package-selected-packages '(helm-ag web-mode tide prettier-js deft company)))
    (custom-set-faces
     )


<a id="org385d4e6"></a>

# Common Lisp Setup

    (setq byte-compile-warnings '(cl-functions))

